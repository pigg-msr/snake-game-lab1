﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnekGame
{
    public class Snake : MonoBehaviour
    {
        private Vector2 gridPosition;
        private Vector2 gridMoveDirection;
        private float gridMoveTimer;
        private float gridMoveTimerMax;

        private void Awake()
        {
            gridPosition = new Vector2(.5f, .5f);
            gridMoveTimerMax = 0.5f;
            gridMoveTimer = gridMoveTimerMax;
            gridMoveDirection = new Vector2(1, 0);
        }

        private void Update()
        {
            HandleInput();
            HandleMovment();
        }
        
        private void HandleInput()
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                if(gridMoveDirection.y != -1)
                {
                    gridMoveDirection.x = 0;
                    gridMoveDirection.y = +1;
                }
            }
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                if (gridMoveDirection.y != +1)
                {
                    gridMoveDirection.x = 0;
                    gridMoveDirection.y = -1;
                }
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                if (gridMoveDirection.x != +1)
                {
                    gridMoveDirection.x = -1;
                    gridMoveDirection.y = 0;
                }
            }
            if (Input.GetKeyDown(KeyCode.RightArrow))
            {
                if (gridMoveDirection.x != -1)
                {
                    gridMoveDirection.x = +1;
                    gridMoveDirection.y = 0;
                }
            }
        }

        private void HandleMovment()
        {
            gridMoveTimer += Time.deltaTime;
            if(gridMoveTimer >= gridMoveTimerMax)
            {
                gridMoveTimer -= gridMoveTimerMax;
                gridPosition += gridMoveDirection;
                if (gridPosition.x >= 8.5 || gridPosition.x <= -8.5 || gridPosition.y >= 8.5 || gridPosition.y <= -8.5)
                {
                    gridPosition = new Vector2(.5f, .5f);
                }
                transform.position = new Vector3(gridPosition.x, gridPosition.y);
                transform.eulerAngles = new Vector3(0, 0, GetAngle(gridMoveDirection)-90);
            }

            
                
        }

        private float GetAngle(Vector2 dir)
        {
            float n = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            if (n < 0) n += 360;
            return n;
        }
    }
}

