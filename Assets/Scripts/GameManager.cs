﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnekGame
{
    public class GameManager : MonoBehaviour
    {
        public static int maxHeight = 16;
        public static int maxWidth = 16;

        public Color color1;
        public Color color2;
        GameObject mapObject;
        SpriteRenderer mapRenderer;
        
        void Start()
        {
            CreateMap();
        }

        void CreateMap()
        {
            mapObject = new GameObject("Map");
            mapRenderer = mapObject.AddComponent<SpriteRenderer>();
            
            Texture2D txt = new Texture2D(maxWidth, maxHeight);

            for (var x = 0; x < maxWidth; x++)
            {
                for (var y = 0; y < maxHeight; y++)
                {
                    #region Visual
                    if (x % 2 != 0)
                    {
                        txt.SetPixel(x, y, y % 2 != 0 ? color1 : color2);
                    }
                    else
                    {
                        txt.SetPixel(x, y, y % 2 == 0 ? color1 : color2);
                    }
                    #endregion
                }
            }

            txt.filterMode = FilterMode.Point;
            txt.Apply();
            
            Rect rect = new Rect(0,0, maxWidth, maxHeight);
            Sprite sprite = Sprite.Create(txt, rect, Vector2.one * .5f, 1, 0, SpriteMeshType.FullRect);
            mapRenderer.sprite = sprite;
        }
    }

}


